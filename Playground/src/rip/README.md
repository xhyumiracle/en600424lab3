# RIP
## Overview
Here is all version of RIP layer

## Current version
### version number: 4
* Passed all the test including all tests under err_rate=90
* Passed interoperability test and shutdown cleanly
* Main stack
    * RIPStackNew
* Relative files:
    * RIPBaseProtocol.py
    * RIPClientProtocolNew.py
    * RIPServerProtocolNew.py
    * RIPTransportNew.py
    * RIPStackNew.py
    * RIPConfig.py
    * RIPMessage.py
    * RIPCrypto.py
    * CertFactory.py
* Written under Ubuntu 16.04 64 bits
* Use state machine to reframe the rip stack
* Modified Playground files:
    * Timer.py
        * Changed OneShotTimer into reusable timer
    * StateMachine.py
        * Commented logs about entering and exiting.

## History versions
### version number: 3
* Cannot passed interoperability test and not shutdown cleanly
### version number: 2
* Simplify version 1, with some hard code error that prof. seth cannot run it on test, thus converted to version 3
### version number: 1
* Try to implement slide window but cannot pass big data tests, thus converted to version 2

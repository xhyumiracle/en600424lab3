HANDSHAKE_STEP0 = 0
HANDSHAKE_STEP1 = 1
DATA_TRANSPORT = 2
QUESTIONING = 3

DEBUG = 0
STRICT = 1
MAX_SEQUENCE_NUMBER = 0xffff
MAX_NONCE_NUMBER = 0xffff
MAX_SEGMENT_SIZE = 2048
SENDER_WINDOW_SIZE = 20480
RECEIVER_WINDOW_SIZE = 20480000
TIMEOUT_HANDSHAKE = 1
TIMEOUT = 1
ACK_SEND_TIMEOUT = 0.01
TIMEOUT_CLOSE = 3
SENDER_BUFFER_TIMER_NUMBER = 100000
MAX_ACK_TOBE_SENT_SIZE = 1000

ERROR_TEST = 0
ERROR_RATE = 30

import random
import time

class RipLogger():
    agent='server or client'
    peerAddr = 'peerAddr'
    logfile = "rip.log"
    toFile = False
    def setAgent(self, type):
        if type == 'server':
            self.agent = 'server'
            self.logfile = 'rip.server.log'
        elif type == 'client':
            self.agent = 'client'
            self.logfile = 'rip.client.log'
        else:
            self.log('err', 'you init me (I\'m logger) with neither server nor client, are you kidding me???')

    def setPeer(self, peerAddr):
        self.peerAddr = peerAddr

    def __beautify_print_rip(self, ripMessage):
        ripstr = ""
        ripstr += '>ACK flag:' + str(ripMessage.acknowledgement_flag) + '\n'
        ripstr += '>ACK num:' + str(ripMessage.acknowledgement_number) + '\n'
        ripstr += '>SEQ num:' + str(ripMessage.sequence_number) + '\n'
        # ripstr += '>Certs:' + str(ripMessage.certificate) + '\n'
        ripstr += '>FIN flag:' + str(ripMessage.close_flag) + '\n'
        # ripstr += '>Session ID:' + ripMessage.sessionID + '\n'
        # ripstr += '>signature:      ' + ripMessage.signature + '\n'
        ripstr += '>data:' + ripMessage.data
        return ripstr

    def printlog(self, logstr):
        x = time.localtime(time.time())
        if self.toFile:
            with open(self.logfile, 'a') as f:
                f.write(time.strftime("[%m/%d/%y %H:%M:%S]", x) + '[%s]' % self.peerAddr + logstr + '\n')
        else:
            print time.strftime("[%m/%d/%y %H:%M:%S]", x) + '[%s]' % self.peerAddr + logstr

    def log(self, type, data = '', state='', sig=''):
        ripstr = '{rip}{%s}' % self.agent
        logstr = '%sNull log, how can this be ??? You did write out a bug dude!' % ripstr
        if type == 'con':
            logstr = ripstr + '{CON}'
        elif type == 'rcv':
            logstr = ripstr + '{RCV}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'snd':
            logstr = ripstr + '{SND}' + '\n' + self.__beautify_print_rip(data)
        elif type == 'err':
            logstr = '\x1b[6;30;42m' + ripstr + '{ERROR}' + data + '\x1b[0m'
        elif type == 'hs':
            logstr = ripstr + '{Handshake}' + data
        elif type == 'sm':
            logstr = ripstr + '{State Machine}' + 'st:%s sig:%s '%(state, sig) + data
        elif type == 'info':
            logstr = ripstr + '{info}' + data
        elif type == 'rip':  # certain ripMessage
            logstr = ripstr + '{RIP}' + '\n' + self.__beautify_print_rip(data)
        if type in ['err', 'rip']:
            self.printlog(logstr)
            pass
        elif type in ['snd', 'rcv']:
            self.printlog(logstr)
            pass
        elif type in ['hs']:
            self.printlog(logstr)
            pass
        elif type in ['sm']:
            # self.printlog(logstr)
            pass
        else:
            # self.printlog(logstr)
            pass

def randomErrorHappen():
    return random.randint(0, 100) < ERROR_RATE

# signals for state machine
class RipSignal():
    open = 0
    rcv2nd = 1
    listen = 2
    rcv1st = 3
    sntcls = 5
    rcvcls = 6
    receive = 7
    establish = 8
    retnml = 9 # return normal from questioning to established

logger = RipLogger()
signal = RipSignal()

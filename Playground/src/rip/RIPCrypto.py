from Crypto.Signature import PKCS1_v1_5
from Crypto.PublicKey import RSA
from Crypto.Hash import SHA256
from playground.crypto import X509Certificate
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher

class RipCrypto():
    @classmethod
    def getRsaObjectFromBytes(cls, rawKeyBytes):
        return RSA.importKey(rawKeyBytes)

    @classmethod
    def getX509ObjectFromBytes(cls, rawCertBytes):
        return X509Certificate.loadPEM(rawCertBytes)

    def __init__(self, rawKey):
        self.rsa = self.importSk(rawKey)

    def importSk(self, rawKey):
        # with open(path) as f:
        #     rawKey = f.read()
        rsaKey = RSA.importKey(rawKey)
        rsaSigner = PKCS1_v1_5.new(rsaKey)
        return rsaSigner

    def sign(self, data):
        hasher = SHA256.new()
        hasher.update(data)
        signatureBytes = self.rsa.sign(hasher)
        return signatureBytes

    def loadCert(self, certBytes):
        cert = X509Certificate.loadPEM(certBytes)
        return cert

    def verifySignature(self, certBytes, data, signature):
        cert = self.loadCert(certBytes)
        peerPublicKeyBlob = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBlob)
        rsaVerifier = PKCS1_v1_5.new(peerPublicKey)
        hasher = SHA256.new()
        hasher.update(data)
        result = rsaVerifier.verify(hasher, signature)
        return result

    def getCommonName(self, certBytes):
        cert = self.loadCert(certBytes)
        return cert.getSubject()["commonName"]

    def verifyCertChain(self, rootCertBytes, certBytes):
        # TODO: verify the intermediate cert by root's public key
        cert = self.loadCert(certBytes)
        rootCert = self.loadCert(rootCertBytes)
        if (cert.getIssuer() != rootCert.getSubject()):
            return False
        # now let's check the signature
        rootPkBytes = rootCert.getPublicKeyBlob()
        # use rootPkBytes to get a verifiying RSA key
        # not shown here. Use the code above
        rootPk = RSA.importKey(rootPkBytes)
        rootVerifier = PKCS1_v1_5.new(rootPk)

        bytesToVerify = cert.getPemEncodedCertWithoutSignatureBlob()
        hasher = SHA256.new()
        hasher.update(bytesToVerify)
        if not rootVerifier.verify(hasher, cert.getSignatureBlob()):
            return False
        return True

    def getPeerPkEncrypter(self, certBytes):
        cert = self.loadCert(certBytes)
        peerPublicKeyBytes = cert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBytes)
        peerRsaEncrypter = PKCS1OAEP_Cipher(peerPublicKey, None, None, None)
        return peerRsaEncrypter

    def getPeerPkDecrypter(self, keyBytes):
        key = RSA.importKey(keyBytes)
        rsaDecrypter = PKCS1OAEP_Cipher(key, None, None, None)
        return rsaDecrypter

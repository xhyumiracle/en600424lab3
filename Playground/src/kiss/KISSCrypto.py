from Crypto.PublicKey import RSA
from Crypto.Random.random import StrongRandom
from Crypto.Cipher import AES
from Crypto.Util import Counter
from Crypto.Cipher.PKCS1_OAEP import PKCS1OAEP_Cipher
from KISSConfig import logger

class KissCrypto():
    rsa = None
    peerRsa = None
    aes = None
    peerAes = None
    aesKey = None
    aesIV = None
    peerAesKey = None
    peerAesIV = None
    def __init__(self, hostPair, peerPair, agent):
        print hostPair.certificateChain
        print peerPair.certificateChain
        skObj = hostPair.privateKey
        self.rsa = PKCS1OAEP_Cipher(skObj, None, None, None)

        peerCert = peerPair.certificateChain[0]
        peerPublicKeyBytes = peerCert.getPublicKeyBlob()
        peerPublicKey = RSA.importKey(peerPublicKeyBytes)
        self.peerRsa= PKCS1OAEP_Cipher(peerPublicKey, None, None, None)

        logger.setAgent(agent)

    def genAESKeyAndIV(self):
        key_size = 32
        IV_size = 16
        sr = StrongRandom()
        key = "".join(chr(sr.randint(0, 0xff)) for i in range(key_size))
        IV = "".join(chr(sr.randint(0, 0xff)) for i in range(IV_size))
        return key, IV

    def setHostAES(self, key, iv):
        IV_asInt = int(iv.encode('hex'), 16)
        IV_asCtr = Counter.new(128, initial_value=IV_asInt)
        self.aes = AES.new(key, counter=IV_asCtr, mode=AES.MODE_CTR)

    def setPeerAES(self, key, iv):
        IV_asInt = int(iv.encode('hex'), 16)
        IV_asCtr = Counter.new(128, initial_value=IV_asInt)
        self.peerAes = AES.new(key, counter=IV_asCtr, mode=AES.MODE_CTR)

    # use peer rsa to encrypt
    def encryptRSA(self, plaintext):
        if not self.peerRsa.can_encrypt():
            logger.log('err', 'self.peerRsa cannot encrypt message')
            return
        return self.peerRsa.encrypt(plaintext)

    # use host rsa to decrypt
    def decryptRSA(self, ciphertext):
        # if not self.rsa.can_decrypt():
        #     logger.log('err', 'self.rsa cannot decrypt message')
        #     return
        return self.rsa.decrypt(ciphertext)

    #use host aes to encrypt
    def encryptAES(self, plaintext):
        return self.aes.encrypt(plaintext)

    #use peer aes to decrypt
    def decryptAES(self, ciphertext):
        return self.peerAes.decrypt(ciphertext)


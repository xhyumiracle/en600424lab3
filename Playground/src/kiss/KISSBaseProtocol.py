from playground.network.common.Protocol import StackingProtocolMixin
from twisted.internet.protocol import Protocol
from playground.network.message.ProtoBuilder import MessageDefinition
from KISSMessage import KissHandShake, KissData
from KISSTransport import KissTransport
from KISSConfig import HANDSHAKE, DATATRANSMISSION, logger
from playground.network.common.Timer import callLater
from KISSCrypto import KissCrypto

class KissBaseProtocol(StackingProtocolMixin, Protocol):
    status = HANDSHAKE
    recBuffer = ""
    hostPair = []
    peerPair = []
    aesKey = ""
    aesIV = ""
    agent = 'server or client'

    def __init__(self, agent):
        self.agent = agent
        logger.setAgent(agent)
        logger.log('info', 'init')

    def hsSendPacket(self):
        k_enc = self.crypto.encryptRSA(self.aesKey)
        IV_enc = self.crypto.encryptRSA(self.aesIV)
        kissHandshake = KissHandShake()
        kissHandshake.key = k_enc
        kissHandshake.IV = IV_enc
        # kissHandshake.key = self.aesKey
        # kissHandshake.IV = self.aesIV
        self.higherTransport.writeKissMessage(kissHandshake)
        logger.log('hs', 'sent handshake packet')

    def hsRecvPacket(self, kissHandshake):
        if not isinstance(kissHandshake, KissHandShake):
            logger.log('err', 'we received an un-handshake packet during handshake')
            return
        else:
            logger.log('hs', 'received handshake packet')
            peerKey = self.crypto.decryptRSA(kissHandshake.key)
            peerIV = self.crypto.decryptRSA(kissHandshake.IV)
            # peerKey = kissHandshake.key
            # peerIV = kissHandshake.IV
            self.crypto.setPeerAES(peerKey, peerIV)

    def recvData(self, kissData):
        if not isinstance(kissData, KissData):
            logger.log('we received an un-data packet during data transmission')
            return
        data = self.crypto.decryptAES(kissData.data)
        self.higherProtocol() and self.higherProtocol().dataReceived(data)

    def receivePacket(self, kissMessage):
        if self.status == HANDSHAKE:
            self.hsRecvPacket(kissMessage)
        elif self.status == DATATRANSMISSION:
            self.recvData(kissMessage)
        else:
            logger.log('err', 'status neither handshake nor data transmission, are you kidding me ???')

    def dataReceived(self, data):
        self.recBuffer += data
        try:
            kissMessage, byteUsed = MessageDefinition.Deserialize(self.recBuffer)
            self.recBuffer = self.recBuffer[byteUsed:]
        except Exception, e:
            return
        self.receivePacket(kissMessage)
        self.recBuffer and self.dataReceived('')
        # self.recBuffer and callLater(0, self.dataReceived, '')

    def connectionMade(self):
        logger.log('info', 'connection made')
        self.higherTransport = KissTransport(self.transport, self)
        self.hostPair = self.higherTransport.getHost()
        self.peerPair = self.higherTransport.getPeer()
        self.crypto = KissCrypto(self.hostPair, self.peerPair, self.agent)
        self.aesKey, self.aesIV = self.crypto.genAESKeyAndIV()
        self.crypto.setHostAES(self.aesKey, self.aesIV)

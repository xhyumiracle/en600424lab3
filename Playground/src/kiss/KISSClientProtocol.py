from KISSBaseProtocol import KissBaseProtocol
from KISSConfig import HANDSHAKE,DATATRANSMISSION
from KISSMessage import KissHandShake

class KissClientProtocol(KissBaseProtocol):
    def __init__(self):
        super(KissClientProtocol, self).__init__('client')

    def hsRecvPacket(self, kissMessage):
        super(KissClientProtocol, self).hsRecvPacket(kissMessage)
        self.status = DATATRANSMISSION
        self.makeHigherConnection(self.higherTransport)

    def connectionMade(self):
        super(KissClientProtocol, self).connectionMade()
        self.hsSendPacket()

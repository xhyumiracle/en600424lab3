from playground.network.common.Protocol import StackingTransport
from KISSMessage import KissData

class KissTransport(StackingTransport):
    protocol = None
    def __init__(self, lowerTransport, protocol):
        StackingTransport.__init__(self, lowerTransport)
        self.protocol = protocol

    def write(self, data):
        kissData = KissData()
        kissData.data = self.protocol.crypto.encryptAES(data)
        self.writeKissMessage(kissData)

    def writeKissMessage(self, kissMessage):
        self.lowerTransport().write(kissMessage.__serialize__())
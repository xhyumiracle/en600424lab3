from twisted.internet.protocol import Factory
from playground.network.common.Protocol import  StackingFactoryMixin
from KISSClientProtocol import KissClientProtocol
from KISSServerProtocol import KissServerProtocol

class KissClientFactory(StackingFactoryMixin, Factory):
    protocol = KissClientProtocol


class KissServerFactory(StackingFactoryMixin, Factory):
    protocol = KissServerProtocol


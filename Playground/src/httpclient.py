import sys

from twisted.internet import reactor, stdio
from twisted.internet.protocol import Protocol, ClientFactory
from twisted.protocols.basic import LineReceiver

# from rip import RIPStack
import lab2stack
from playground.twisted.endpoints import GateClientEndpoint

global hoststr, httpprotocol
host = 'localhost'
port = 8124
hoststr = host + ':' + str(port)
httpprotocol = object


class HttpProtocol(Protocol):
    global hoststr
    flag = True
    httphead = 'GET {} HTTP/1.{}\r\nHost: {}\r\n\r\n'

    def __init__(self):
        self.dataSend = self.sendHttpGet

    def connectionMade(self):
        self.log(hoststr, 'con')
        print '>>>'
        # cmd = raw_input()
        # while (cmd == ''): cmd = raw_input()
        # self.sendMessage(self.sendHttpGet(cmd, '1')) # second arg is x for HTTP 1.x

        # self.sendMessage('Nice to meet you!')

    def sendMessage(self, data):
        self.log(hoststr, 'snd', data)
        self.transport.write(data)

    def sendHttpGet(self, filepath, host = hoststr, version = '1'):
        self.sendMessage(self.httphead.format(filepath, version, host)) # second arg is x for HTTP 1.x

    def dataReceived(self, data):
        self.log(hoststr, 'rcv', data)
        sys.stdout.write(data)
        sys.stdout.write('>>>')
        sys.stdout.flush()

    def log(self, server, type, data = ''):
        serveraddr = '[' + server + ']'
        if type == 'con':
            logstr = '[CON]' + 'connected to ' + serveraddr
        elif type == 'rcv':
            logstr = '[RCV]' + serveraddr + '\n' + data
        elif type == 'snd':
            logstr = '[SND]' + serveraddr + '\n' + data
        elif type == 'req':
            logstr = '[REQ]' + serveraddr + data
        # print logstr


class HttpFactory(ClientFactory):
    global host, port, httpprotocol

    def startedConnecting(self, connector):
        print 'Connecting to ' + hoststr

    def buildProtocol(self, addr):
        return httpprotocol

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection ', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection Failed ', reason

class StdIO(LineReceiver):
    global httpprotocol
    delimiter = '\n'

    def connectionMade(self):
        pass

    def lineReceived(self, line):
        if line == 'q':
            httpprotocol.transport.loseConnection()
            return
        httpprotocol.transport and httpprotocol.sendHttpGet(line)

httpprotocol = HttpProtocol()
#d = reactor.connectTCP(host, port, HttpFactory())
stdio.StandardIO(StdIO())
#
endpoint=GateClientEndpoint.CreateFromConfig(reactor,"20164.1.47806.1", port,"xhyumiracle", networkStack=lab2stack)
endpoint.connect(HttpFactory())
reactor.run()

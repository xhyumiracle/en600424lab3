# EN600424Lab2
## Installation
* `$ git clone git@bitbucket.org:xhyumiracle/en600424lab3.git`
* Please do include both `en600424lab3/Playground` and `en600424lab3/Playground/src` in ***PYTHONPATH***

## Introduction
### KISS version 1
* Directory
    * EN400626Lab3/Playground/src/kiss
* Main stack
    * KISSStackNew
* Relative files:
    * KISSStack.py
    * KISSBaseProtocol.py
    * KISSClientProtocol.py
    * KISSServerProtocol.py
    * KISSCrypto.py
    * KISSTransport.py
    * KISSMessage.py
    * KISSConfig.py
* Written under Ubuntu 16.04 64 bits

### RIP version 3
* Passed all the test including all tests under err_rate=90
* Passed interoperability test and shutdown cleanly
* Directory
    * EN400626Lab3/Playground/src/rip
* Main stack
    * RIPStackNew
* Relative files:
    * RIPBaseProtocol.py
    * RIPClientProtocolNew.py
    * RIPServerProtocolNew.py
    * RIPTransportNew.py
    * RIPStackNew.py
    * RIPConfig.py
    * RIPMessage.py
    * RIPCrypto.py
    * CertFactory.py
* Written under Ubuntu 16.04 64 bits
* Use state machine to reframe the rip stack
* Modified Playground files:
    * Timer.py
        * Changed OneShotTimer into reusable timer
    * StateMachine.py
        * Commented logs about entering and exiting.

